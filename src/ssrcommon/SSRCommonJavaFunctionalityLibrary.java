/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;

/**
 *
 * @author Siddhartha
 * Created on : 22nd Dec 2016
 * Purpose : This class will contain general functionality at java level
 */
public class SSRCommonJavaFunctionalityLibrary 
{
    public static String getDisplayTimeStampFromCalendar(Calendar i_calendarToConvert)
            throws Exception
    {
        String yearInString = (new Integer(i_calendarToConvert.get(Calendar.YEAR))).toString() ;
        Integer monthInInteger = i_calendarToConvert.get(Calendar.MONTH) + 1 ;
        String monthInStringPadded = ((monthInInteger < 10) ? "0" : "") + monthInInteger.toString() ;
        Integer dayInInteger = i_calendarToConvert.get(Calendar.DAY_OF_MONTH) ;
        String dayInString = ((dayInInteger < 10) ? "0" : "") + dayInInteger.toString() ;
        String hourInString = (new Integer(i_calendarToConvert.get(Calendar.HOUR_OF_DAY))).toString() ;
        String minutesInString = (new Integer(i_calendarToConvert.get(Calendar.MINUTE))).toString() ;
        String secondsInString = (new Integer(i_calendarToConvert.get(Calendar.SECOND))).toString() ; 
        String toReturn = yearInString + monthInStringPadded + dayInString + " "
                + hourInString + ":" + minutesInString + ":" + secondsInString ;
        return toReturn ;
    }
    
    public static Vector<String> getStandardizedTokensFromString(
            String i_stringToTokenize, String i_separatorRegexp) throws Exception
    {
        Vector<String> toReturn = new Vector<String>() ;
        toReturn.addAll(Arrays.asList(i_stringToTokenize.split(i_separatorRegexp))) ;
        while(true == toReturn.contains(""))
        {
            toReturn.remove("") ;
        }
        return toReturn ;
    }
}
