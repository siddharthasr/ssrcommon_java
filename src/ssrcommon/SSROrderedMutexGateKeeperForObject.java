/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon;

/**
 *
 * @author LENOVO
 */
public class SSROrderedMutexGateKeeperForObject <T>
{
    protected T m_protectedObject ;
    public int m_mutexWaitDuration ;
    protected SSROrderedReadWriteMutex m_orderedReadWriteMutex ;
    
    public SSROrderedMutexGateKeeperForObject(T i_protectedObject)
    {
        this.m_protectedObject = i_protectedObject ;
        this.m_orderedReadWriteMutex = new SSROrderedReadWriteMutex() ;
        this.m_mutexWaitDuration = 100 ;
    }
    
    public <R> void performMutexProtectedOperation(SSRMutexProtectedOperation<T, R> i_operation,
            SSROrderedReadWriteMutex.ResourceOperation i_resourceOperation)
            throws Exception 
    {
        Boolean mutexAcquired = false;
        try 
        {
            this.m_orderedReadWriteMutex.acquire(i_resourceOperation, this.m_mutexWaitDuration);
            mutexAcquired = true;

            i_operation.executeOperation(this.m_protectedObject);
        } 
        catch (Exception e) 
        {
            throw e;
        } 
        finally 
        {
            if (true == mutexAcquired) 
            {
                this.m_orderedReadWriteMutex.release();
            }
        }
    }
}
