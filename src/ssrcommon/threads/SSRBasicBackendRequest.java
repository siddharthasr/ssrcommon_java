package ssrcommon.threads;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 12/7/2016.
 * Purpose : This class is the basic request class for backend service / servers
 * Changelog:
 * 1) 20170206 -
 * In the readRequestFromInputStream, the object read for m_requestData was being
 * cast to Object[] before being assigned to m_requestData. This was causing problem
 * when it was not an object array, but was a request data entity. Anyways, it was
 * not needed because the member m_requestData is actually an Object and not an
 * Object array. Fixed this.
 */
public class SSRBasicBackendRequest implements ISSRBackendRequest
{
    public String m_requestId ;
    public String m_serviceRequestCode ;
    public Integer m_requestDataSize ;
    public Object m_requestData ;

    @Override
    public void readRequestFromInputStream(InputStream i_inputStreamToReadFrom) throws Exception
    {
        // It turns out that a lot of times I am sending ObjectInputStream instead
        // of an InputStream object. While strictly speaking, it should still work,
        // but it doesn't. Hence we make sure that the class we are receiving here
        // is InputStream or throw Exception otherwise.
        if(true == i_inputStreamToReadFrom instanceof ObjectInputStream)
        {
            throw new SSRBasicException("Invalid class passed as argument. Input class \""
            + i_inputStreamToReadFrom.getClass().toString() + "\"") ;
        }
        
        ObjectInputStream objectInputStreamToUse = new ObjectInputStream(i_inputStreamToReadFrom) ;

        this.m_requestId = (String)objectInputStreamToUse.readObject() ;
        this.m_serviceRequestCode = (String)objectInputStreamToUse.readObject() ;
        this.m_requestDataSize = (Integer)objectInputStreamToUse.readObject() ;
        this.m_requestData = (Object)objectInputStreamToUse.readObject() ;
    }

    @Override
    public void writeRequestToOutputStream(OutputStream i_outputStreamToWriteTo) throws Exception
    {
        // It turns out that a lot of times I am sending ObjectOutputStream instead
        // of an OutputStream object. While strictly speaking, it should still work,
        // but it doesn't. Hence we make sure that the class we are receiving here
        // is OutputStream or throw Exception otherwise.
        if(true == i_outputStreamToWriteTo instanceof ObjectOutputStream)
        {
            throw new SSRBasicException("Invalid class passed as argument. Input class \""
            + i_outputStreamToWriteTo.getClass().toString() + "\"") ;
        }
        
        ObjectOutputStream objectOutputStreamToUse = new ObjectOutputStream(i_outputStreamToWriteTo) ;

        objectOutputStreamToUse.writeObject(this.m_requestId) ;
        objectOutputStreamToUse.writeObject(this.m_serviceRequestCode) ;
        objectOutputStreamToUse.writeObject(this.m_requestDataSize) ;
        objectOutputStreamToUse.writeObject(this.m_requestData) ;
    }
    
    @Override
    public String toString()
    {
        String toReturn = "" ;
        
        String requestId = (null == this.m_requestId ? "<null>" : this.m_requestId) ;
        String requestIdClass = (null == this.m_requestId ? "<null>" : this.m_requestId.getClass().getCanonicalName()) ;
        String serviceRequestCode = (null == this.m_serviceRequestCode ? "<null>" : this.m_serviceRequestCode) ;
        String serviceRequestCodeClass = (null == this.m_serviceRequestCode ? "<null>" : this.m_serviceRequestCode.getClass().getCanonicalName()) ;
        String requestDataSize = (null == this.m_requestDataSize ? "<null>" : this.m_requestDataSize.toString()) ;
        String requestDataSizeClass = (null == this.m_requestDataSize ? "<null>" : this.m_requestDataSize.getClass().getCanonicalName()) ;
        String requestData = (null == this.m_requestData ? "<null>" : this.m_requestData.toString()) ;
        String requestDataClass = (null == this.m_requestData ? "<null>" : this.m_requestData.getClass().getCanonicalName()) ;
        
        toReturn += "\nm_requestId (" + requestIdClass + "): " + requestId ;
        toReturn += "\nm_serviceRequestCode (" + serviceRequestCodeClass + "): " + serviceRequestCode ;
        toReturn += "\nm_requestDataSize (" + requestDataSizeClass + "): " + requestDataSize ;
        String requestDataCanonicalName = (null != requestDataClass) ? requestDataClass : "anynymous class" ;
        toReturn += "\nm_requestData (" + requestDataCanonicalName + "): " + requestData ;

        return toReturn ;
    }
}
