package ssrcommon.threads;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created on 12/6/2016.
 * Purpose : This is supposed to be the interface for the response classes for backend.
 */
public interface ISSRBackendResponse
{
    public void readResponseFromInputStream(InputStream i_inputStreamToReadFrom) throws Exception ;

    public void writeResponseToOutputStream(OutputStream i_outputStreamToWriteTo) throws Exception ;
    
    public void populateWithFailureResponseData(Object i_failureIdentifierParameter,
            Object i_failureDetailsParameter) throws Exception ;
}
