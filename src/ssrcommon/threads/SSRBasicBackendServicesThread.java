package ssrcommon.threads;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import ssrcommon.SSRCommonJavaFunctionalityLibrary;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 7/12/2016.
 * Changelog:
 * 1) 20170206 -
 * Moved the socket used for connection to member variable so that it is accessible
 * for further use, wherever the object for this thread is held. Also created a
 * boolean member variable to be able to tell whether the flow completed properly
 * or not.
 * 2) 20170214 -
 * Added the functionality for indicating the status of the thread.
 * Also added functionality to receive the socket to use, in the input parameters.
 */
public class SSRBasicBackendServicesThread extends SSRThreadForBackgroundWork
{
    public static final String VERSION = "v1.0 20170207" ;
    public static final Integer DEFAULT_SOCKET_CONNECTION_TIMEOUT_DURATION = 10000 ;
    
    public static final String KEY_SERVER_IP_ADDRESS = "BACKEND_SERVER_IP_ADDRESS" ;
    public static final String KEY_SERVER_PORT_NUMBER = "BACKEND_SERVER_PORT_NUMBER" ;
    
    public static final String SOCKET_FOR_BACKEND_PARAMETER_NAME = "SOCKET_FOR_BACKEND_PARAMETER" ;
    
    // Abort means a graceful exit, without finishing, from a flow which was not 
    // success. Exact criteria for what constitutes as "graceful" exit. Should be decided.
    public enum ThreadFlowStatus {INITIAL, READY, RUNNING, FINISH_SUCCESSFULLY, FINISH_FAILURE, ABORTED} ;
    
    public ISSRBackendRequest m_requestToBackend ;
    public ISSRBackendResponse m_responseFromBackend ;
    
    public ThreadFlowStatus m_status ;
    public Socket m_socketToBackend ;
    
    public Integer m_socketConnectionTimeoutInMilliseconds ;

    // The third parameter null is being allowed here for the case that the user
    // wants to set the parameters hashmap after the creation of the object.
    // This class itself assumes that the IP address and the port address will be
    // provided in the parameters HashMap and so if it is null the object created
    // is not usable.
    public SSRBasicBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                         ISSRBackendResponse o_responseFromBackend)
            throws Exception
    {
        this(i_requestToBackend, o_responseFromBackend, null) ;
    }
    
    public SSRBasicBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                         ISSRBackendResponse o_responseFromBackend,
                                         HashMap<Object, Object> i_parametersHashMap)
            throws Exception
    {
        // Instantiate the object with same parameters, and default timeout value of 10 seconds.
        this(i_requestToBackend, o_responseFromBackend, i_parametersHashMap, 
                SSRBasicBackendServicesThread.DEFAULT_SOCKET_CONNECTION_TIMEOUT_DURATION) ;
    }
    
    public SSRBasicBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                         ISSRBackendResponse o_responseFromBackend,
                                         HashMap<Object, Object> i_parametersHashMap,
                                         Integer i_socketTimeoutToSet)
            throws Exception
    {
        super(i_parametersHashMap) ;
        this.m_requestToBackend = i_requestToBackend ;
        this.m_responseFromBackend = o_responseFromBackend ;
        this.m_socketConnectionTimeoutInMilliseconds = i_socketTimeoutToSet ;
        this.m_status = SSRBasicBackendServicesThread.ThreadFlowStatus.READY ;
    }

    @Override
    public void runMainLogic() throws Exception
    {
        String oneLogMessage = "Entering SSRBasicBackendServicesThread.runMainLogic" ;
        this.logMessage(oneLogMessage, null) ;
        
        this.handleConnectionEstablishment();
        
        m_requestToBackend.writeRequestToOutputStream(this.m_socketToBackend.getOutputStream());
        
        oneLogMessage = "Request sent to backend server." ;
        this.logMessage(oneLogMessage, null) ;
        
        m_responseFromBackend.readResponseFromInputStream(this.m_socketToBackend.getInputStream());
        
        oneLogMessage = "Response received from backend server." ;
        this.logMessage(oneLogMessage, null) ;
        
        // If flow has reached this point, only then we will consider that the flow
        // completed successfully
        this.m_status = SSRBasicBackendServicesThread.ThreadFlowStatus.FINISH_SUCCESSFULLY ;
    }
    
    @Override
    protected void prepareForThreadFlow() throws Exception
    {
        // If the thread is not in ready state, stop execution.
        if(SSRBasicBackendServicesThread.ThreadFlowStatus.READY != this.m_status)
        {
            throw new SSRBasicException("ThreadFlowStatus is not ready. Please check.") ;
        }
        this.m_status = SSRBasicBackendServicesThread.ThreadFlowStatus.RUNNING ;
    }
    
    @Override
    protected void finishThreadFlowHandling() throws Exception
    {
        // If the thread is running, i.e. has not already been marked for failure.
        // mark the status as success.
        if(this.m_status == SSRBasicBackendServicesThread.ThreadFlowStatus.RUNNING)
            this.m_status = SSRBasicBackendServicesThread.ThreadFlowStatus.FINISH_SUCCESSFULLY ;
    }
    
    @Override
    protected void handleThreadFlowException(String i_errorMessage, Exception i_exceptionToHandle)
    {
        this.m_status = SSRBasicBackendServicesThread.ThreadFlowStatus.FINISH_FAILURE ;
        super.handleThreadFlowException(i_errorMessage, i_exceptionToHandle);
        return ;
    }
    
    protected void handleConnectionEstablishment() throws Exception
    {
        if(true == this.m_parametersHashMap.containsKey(SSRBasicBackendServicesThread
                .SOCKET_FOR_BACKEND_PARAMETER_NAME))
        {
            this.m_socketToBackend = (Socket)this.m_parametersHashMap.get(
                    SSRBasicBackendServicesThread.SOCKET_FOR_BACKEND_PARAMETER_NAME) ;
        }
        else
        {
            this.m_socketToBackend = new Socket();
            try 
            {
                this.m_socketToBackend.connect(this.getInetSocketAddressForConnection(), 
                        this.m_socketConnectionTimeoutInMilliseconds);
            } 
            catch (Exception e) 
            {
                this.showExceptionError("Socket connection error : \n", e);
                this.m_responseFromBackend.populateWithFailureResponseData(
                        SSRBasicBackendResponse.SOCKET_TIMEOUT_ERROR, null);
                return;
            }

            this.logMessage("Connected to backend server.");
        }
    }
    
    protected InetSocketAddress getInetSocketAddressForConnection() throws Exception
    {
        this.logMessage("Entering SSRBasicBackendServicesThread.getInetSocketAddressForConnection");
        String serverIPAddress = (String) this.m_parametersHashMap.get(
                SSRBasicBackendServicesThread.KEY_SERVER_IP_ADDRESS);
        Integer serverSocketNumber = (Integer) this.m_parametersHashMap.get(
                SSRBasicBackendServicesThread.KEY_SERVER_PORT_NUMBER);

        String oneLogMessage = "IP : " + serverIPAddress
                + " port : " + serverSocketNumber
                + " timeout : " + this.m_socketConnectionTimeoutInMilliseconds + " ms";
        this.logMessage(oneLogMessage, null);
        InetSocketAddress toReturn = new InetSocketAddress(serverIPAddress, serverSocketNumber);
        return toReturn;
    }
}