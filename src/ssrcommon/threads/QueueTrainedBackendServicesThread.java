package ssrcommon.threads;

import java.util.HashMap;
import ssrcommon.SSROrderedMutex;

/**
 * Created on 8/31/2016.
 */
public class QueueTrainedBackendServicesThread extends SSRBasicBackendServicesThread
{
    protected SSROrderedMutex m_queueMutexToUse ;
    
    public QueueTrainedBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                             ISSRBackendResponse o_responseFromBackend,
                                             HashMap<Object, Object> i_parametersHashMap,
                                             SSROrderedMutex i_queueAsMutex)
            throws Exception
    {
        // Instantiate object with default timeout of 10 seconds
        this(i_requestToBackend, o_responseFromBackend, i_parametersHashMap, i_queueAsMutex, 10000) ;
    }

    public QueueTrainedBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                             ISSRBackendResponse o_responseFromBackend,
                                             HashMap<Object, Object> i_parametersHashMap,
                                             SSROrderedMutex i_queueAsMutex,
                                             Integer i_socketTimeoutToSet)
            throws Exception
    {
        super(i_requestToBackend, o_responseFromBackend, i_parametersHashMap, i_socketTimeoutToSet) ;
        this.m_queueMutexToUse = i_queueAsMutex ;
    }

    @Override
    public void run()
    {
        try
        {
            this.m_queueMutexToUse.acquire(100);
            super.run() ;
            this.m_queueMutexToUse.release() ;
        }
        catch (Exception e)
        {
            this.showExceptionError(new String("Exception while trying to "
                    + "acquire m_queueProtectMutex in SSROrderedMutex : "), e) ;
            return ;
        }
    }
}
