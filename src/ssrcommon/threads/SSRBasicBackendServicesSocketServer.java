package ssrcommon.threads;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.* ;
import java.util.GregorianCalendar;
import java.util.HashMap;
import ssrcommon.SSRCommonJavaFunctionalityLibrary;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Purpose : This class is to provide all "under the hood" functioning / handling for
 *           writing the backend servers that I write.
 * 
 * Changelog :
 * 1) 20170118 - 
 * Put a try catch block inside the inner loop of control thread which
 * keeps on looking for incoming control instructions. This was done
 * because otherwise, a single exception in handling of any one control
 * instruction was taking the control thread out of the loop after which the
 * control thread would stop
 * 
 * 2) 20170205 -
 * Modularized the run method implementation. The tasks of creating ServerSocket,
 * starting of control thread, auxiliary handling and starting the server accept 
 * loop were put in separate methods meant for each of them.
 * 
 * 3) 20170207 -
 * Modularized the handleIncomingControlInstruction and added processControlInstrution
 * so that the processing of incoming control instruction can be overloaded if
 * needed in any derived class.
 */

public class SSRBasicBackendServicesSocketServer extends SSRThreadForBackgroundWork
{	
    public static final String SERVER_TO_CONTROL = "SERVER_TO_CONTROL" ;
    public static final String SERVER_CONTROL_SOCKET = "SERVER_CONTROL_SOCKET" ;
    
    public static final String QUIT_SERVER_INSTRUCTION = "QUIT_SERVER_INSTRUCTION" ;
    
    //protected static final Integer MARK_READ_LIMIT = 1024 ;
    
    protected Integer m_serverPort ;
    protected Integer m_serverControlPort ;
    protected ServerSocket m_serverSocket ;
    public Boolean m_stopFlag ;
    
    public SSRBasicBackendServicesSocketServer(Integer i_serverPort)
            throws Exception
    {
        this(i_serverPort, null) ;
    }
    
    public SSRBasicBackendServicesSocketServer(Integer i_serverPort, 
            Integer i_serverControlPort) throws Exception
    {
        this.m_serverPort = new Integer(i_serverPort) ;
        this.m_serverControlPort = new Integer(i_serverControlPort) ;
        this.m_stopFlag = false ;
    }
    
    @Override
    public void run() 
    {
        this.logMessage("SSRBasicBackendServicesSocketServer version 1.1 (20170118)") ;
        
        try
        {
            // create server socket, with technical parameters as needed
            this.createServerSocket() ;

            // start control thread
            this.startControlThread() ;
            
            // perform auxiliary things
            this.performAuxiliaryHandling() ;

            // start server accept loop
            this.startServerAcceptLoop() ;
        }
        catch (Exception e)
        {
            System.out.println("Exception caught in main program loop : \n" 
                               + e.getMessage()) ;
            e.printStackTrace(System.out) ;
            return ;
        }
    }
    
    protected void startControlThread() throws Exception
    {
        try
        {
            if(null != this.m_serverControlPort)
            {
                this.logMessage("Creating control thread on port : " + this.m_serverControlPort) ;
                SSRThreadForBackgroundWork controlThread = this.getControlThread() ;
                controlThread.start() ;
                this.logMessage("Control thread started") ;
            }
        }
        catch(Exception e)
        {
            System.out.println("\nException caught trying to start control thread.") ;
            e.printStackTrace(System.out) ;
            throw e ;
        }
    }
    
    protected void createServerSocket() throws Exception
    {
        try
        {
            this.logMessage("Started in SSRBasicBackendServicesSocketServer.createServerSocket") ;
            this.m_serverSocket = new ServerSocket(this.m_serverPort) ;
            this.logMessage("ServerSocket created on desired port : " + this.m_serverPort) ;
        }
        catch (Exception e)
        {
            // TODO : Do proper exception handling here
            System.out.println("Server socket could not be opened.") ;
            System.out.println("Exception caught while trying to open socket : \n" 
                               + e.getMessage()) ;
            e.printStackTrace(System.out) ;
            throw e ;
        }
    }
    
    protected void performAuxiliaryHandling() throws Exception
    {
        // Empty implementation. This is to be used by derived classes, if needed
    }
    
    protected void startServerAcceptLoop()
    {        
        while (false == this.m_stopFlag)
        {
            System.out.println("Inside server accept loop.") ;
            try
            {
                this.logMessage("stop flag state : " + this.m_stopFlag.toString()) ;
                if(true == this.m_stopFlag)
                {
                    this.logMessage("No more service request to be accepted. Exiting connection accept loop.") ;
                    break ;
                }
                else
                    this.logMessage("Proceed to accepting incoming service request.") ;
                Socket clientSocket = this.m_serverSocket.accept() ;
                SSRBasicBackendServicesWorkerThread oneWorkerThread = 
                                this.getOneWorkerThread(clientSocket) ;
                oneWorkerThread.start() ;
            }
            catch (IOException e)
            {
                // TODO : Do proper exception handling here
                System.out.println("IOException caught in accept loop : " + e.getMessage()) ;
                e.printStackTrace(System.out) ;
            }
            catch (Exception e)
            {
                // TODO : Do proper exception handling here
                System.out.println("Exception caught in accept loop : " + e.getMessage()) ;
                e.printStackTrace(System.out) ;
            }
        }
    }
    
    protected SSRThreadForBackgroundWork getControlThread() throws Exception
    {
        this.logMessage("Entering SSRBasicBackendServicesSocketServer.getControlThread") ;
        ServerSocket serverControlSocket = new ServerSocket(this.m_serverControlPort) ;
        // This thread will create a dummy request for application logic server
        // so that the server actually reruns its internal loop to check the
        // exit boolean and actually quit instead of waiting to get some request
        // so that the actual check can be reached.
        HashMap<Object, Object> controlThreadParams = new HashMap<Object, Object>() ;
        controlThreadParams.put(SSRBasicBackendServicesSocketServer.SERVER_TO_CONTROL, this) ;
        controlThreadParams.put(SSRBasicBackendServicesSocketServer.SERVER_CONTROL_SOCKET, 
                serverControlSocket) ;
        
        SSRThreadForBackgroundWork controlThreadToReturn = 
                new SSRThreadForBackgroundWork(controlThreadParams)
        {
            public void runMainLogic() throws Exception
            {
                SSRBasicBackendServicesSocketServer socketServerToControl =
                        (SSRBasicBackendServicesSocketServer)this.m_parametersHashMap
                                .get(SSRBasicBackendServicesSocketServer
                                .SERVER_TO_CONTROL) ;
                ServerSocket serverControllingSocket = 
                        (ServerSocket) this.m_parametersHashMap
                                .get(SSRBasicBackendServicesSocketServer
                                .SERVER_CONTROL_SOCKET) ;
                        
                this.logMessage("Starting control thread main loop.") ;
                while(true)
                {
                    // It is important to catch this exception and handle it here, because otherwise
                    // error in a single control instruction handling will take the thread out 
                    // of the loop for handling control instructions.
                    try
                    {
                        Socket serverInstructionSocket = serverControllingSocket.accept() ;
                        this.logMessage("Found incoming connection on control thread.") ;
                        socketServerToControl.handleIncomingControlInstruction(
                                serverInstructionSocket) ;
                    }
                    catch(Exception e)
                    {
                        this.handleThreadFlowException("Exception caught in instruction handling " 
                                + "loop of control thread", e);
                    }
                }
            }
        } ;
        
        return controlThreadToReturn ;
    }
    
    // This method will receive all the incoming control instructions for handling
    // if the default control thread is used.
    protected void handleIncomingControlInstruction(Socket i_incomingSocket) throws Exception
    {
        this.logMessage("Entering SSRBasicBackendServicesSocketServer.handleIncomingControlInstruction") ;
        ObjectInputStream incomingInstructionStream = new ObjectInputStream(
                i_incomingSocket.getInputStream()) ;

        String oneInstruction = (String)incomingInstructionStream.readObject() ;
        
        this.logMessage("Received one instruction : " + oneInstruction) ;
        
        if(true == oneInstruction.equals(SSRBasicBackendServicesSocketServer
                .QUIT_SERVER_INSTRUCTION))
        {
            this.logMessage("Handling QUIT_SERVER_INSTRUCTION") ;
            this.m_stopFlag = true ;
            ObjectOutputStream responseStream = new ObjectOutputStream(
                    i_incomingSocket.getOutputStream()) ;
            String oneResponseString = "Quit server instruction received." ;
            responseStream.writeObject(oneResponseString) ;
            this.logMessage("Received response sent for QUIT_SERVER_INSTRUCTION") ;
            
            Socket dummySocketToCauseServerExit = new Socket() ;
            
            // TODO : This needs to be fixed so that proper IP and port are taken
            dummySocketToCauseServerExit.connect(new InetSocketAddress("159.203.132.191", 8000),
                    10000) ;
            this.logMessage("dummySocketToCauseServerExit processing done.") ;
        }
        else
        {
            this.processControlInstruction(oneInstruction, incomingInstructionStream) ;
        }
        
        this.logMessage("Exiting SSRBasicBackendServicesSocketServer.handleIncomingControlInstruction") ;
    }
    
    protected void processControlInstruction(String i_instruction, ObjectInputStream 
            i_objectStreamToInstructionSource) throws Exception
    {
        this.logMessage("Entering SSRBasicBackendServicesSocketServer.processControlInstruction");
        
        // Empty implementation. To be implemented by anybody who is interested
    }
    
    protected SSRBasicBackendServicesWorkerThread getOneWorkerThread(Socket i_clientSocket)
            throws Exception
    {
        this.logMessage("Entering SSRBasicBackendServicesSocketServer.getOneWorkerThread") ;
        return new SSRBasicBackendServicesWorkerThread(i_clientSocket) ;
    }
}
