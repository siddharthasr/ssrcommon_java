package ssrcommon.threads;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 12/7/2016.
 * Purpose : This class is the basic response class for backend service / servers
 */
public class SSRBasicBackendResponse implements ISSRBackendResponse
{
    public static final String RESPONSE_OK = "RESPONSE_OKAY" ;
    public static final String RESPONSE_FAILURE = "RESPONSE_FAILURE" ;
    
    public static final String SOCKET_TIMEOUT_ERROR = "CONNECTION_TIMEOUT" ;
    public static final String GENERAL_BACKEND_ERROR = "GENERAL_BACKEND_ERROR" ;
    public static final String UNKNOWN_BACKEND_ERROR = "UNKNOWN_BACKEND_ERROR" ;
    
    public static final String RESPONSE_TIMEOUT = "RESPONSE_TIMEOUT" ;
    
    public String m_responseId ;
    public String m_serverResponseCode ;
    public String m_requestedServiceCode ;
    public Integer m_responseDataSize ;
    public Object m_responseData ;

    @Override
    public void readResponseFromInputStream(InputStream i_inputStreamToReadFrom) throws Exception
    {
        // It turns out that a lot of times I am sending ObjectInputStream instead
        // of an InputStream object. While strictly speaking, it should still work,
        // but it doesn't. Hence we make sure that the class we are receiving here
        // is InputStream or throw Exception otherwise.
        if(true == i_inputStreamToReadFrom instanceof ObjectInputStream)
        {
            throw new SSRBasicException("Invalid class passed as argument. Input class \""
            + i_inputStreamToReadFrom.getClass().toString() + "\"") ;
        }
        
        ObjectInputStream objectInputStreamToUse = new ObjectInputStream(i_inputStreamToReadFrom) ;

        this.m_responseId = (String)objectInputStreamToUse.readObject() ;
        this.m_serverResponseCode = (String)objectInputStreamToUse.readObject() ;
        this.m_requestedServiceCode = (String)objectInputStreamToUse.readObject() ;
        this.m_responseDataSize = (Integer)objectInputStreamToUse.readObject() ;
        this.m_responseData = objectInputStreamToUse.readObject() ;
    }

    @Override
    public void writeResponseToOutputStream(OutputStream i_outputStreamToWriteTo) throws Exception
    {
        // It turns out that a lot of times I am sending ObjectOutputStream instead
        // of an OutputStream object. While strictly speaking, it should still work,
        // but it doesn't. Hence we make sure that the class we are receiving here
        // is OutputStream or throw Exception otherwise.
        if(true == i_outputStreamToWriteTo instanceof ObjectOutputStream)
        {
            throw new SSRBasicException("Invalid class passed as argument. Input class \""
            + i_outputStreamToWriteTo.getClass().toString() + "\"") ;
        }
        
        ObjectOutputStream objectOutputStreamToUse = new ObjectOutputStream(i_outputStreamToWriteTo) ;

        objectOutputStreamToUse.writeObject(this.m_responseId) ;
        objectOutputStreamToUse.writeObject(this.m_serverResponseCode) ;
        objectOutputStreamToUse.writeObject(this.m_requestedServiceCode) ;
        objectOutputStreamToUse.writeObject(this.m_responseDataSize) ;
        objectOutputStreamToUse.writeObject(this.m_responseData) ;
    }
    
    // Try to make sure that this method does not throw any exception.
    @Override
    public void populateWithFailureResponseData(Object i_failureIdentifierParameter,
            Object i_failureDetailsParameter)
    {
        if(null == i_failureIdentifierParameter)
        {
            this.m_serverResponseCode = SSRBasicBackendResponse.UNKNOWN_BACKEND_ERROR ;
            this.m_responseDataSize = 0 ;
            this.m_responseData = this.m_serverResponseCode ;
        }
        
        if(true == i_failureIdentifierParameter.equals(SSRBasicBackendResponse.SOCKET_TIMEOUT_ERROR))
        {
            this.m_serverResponseCode = SSRBasicBackendResponse.RESPONSE_TIMEOUT ;
            this.m_responseDataSize = 0 ;
            this.m_responseData = this.m_serverResponseCode ;
        }
        
        if(true == i_failureIdentifierParameter.equals(SSRBasicBackendResponse.GENERAL_BACKEND_ERROR))
        {
            this.m_serverResponseCode = SSRBasicBackendResponse.GENERAL_BACKEND_ERROR ;
            this.m_responseDataSize = 0 ;
            this.m_responseData = this.m_serverResponseCode ;
        }
        
        this.populateFailureDetailsInResponse(i_failureDetailsParameter) ;
    }
    
    public void populateResponseFieldsWithCorrespondingRequestFields(
            SSRBasicBackendRequest i_requestToCopy)
    {
        this.m_responseId = i_requestToCopy.m_requestId ;
        this.m_requestedServiceCode = i_requestToCopy.m_serviceRequestCode ;
    }
    
    @Override
    public String toString()
    {
        String toReturn = "" ;
        
        String responseId = (null == this.m_responseId ? "<null>" : this.m_responseId) ;
        String responseIdClass = (null == this.m_responseId ? "<null>" : this.m_responseId.getClass().getCanonicalName()) ;
        String serverResponseCode = (null == this.m_serverResponseCode ? "<null>" : this.m_serverResponseCode) ;
        String serverResponseCodeClass = (null == this.m_serverResponseCode ? "<null>" : this.m_serverResponseCode.getClass().getCanonicalName()) ;
        String requestedServiceCode = (null == this.m_requestedServiceCode ? "<null>" : this.m_requestedServiceCode) ;
        String requestedServiceCodeClass = (null == this.m_requestedServiceCode ? "<null>" : this.m_requestedServiceCode.getClass().getCanonicalName()) ;
        String responseDataSize = (null == this.m_responseDataSize ? "<null>" : this.m_responseDataSize.toString()) ;
        String responseDataSizeClass = (null == this.m_responseDataSize ? "<null>" : this.m_responseDataSize.getClass().getCanonicalName()) ;
        String responseData = (null == this.m_responseData ? "<null>" : this.m_responseData.toString()) ;
        String responseDataClass = (null == this.m_responseData ? "<null>" : this.m_responseData.getClass().getCanonicalName()) ;
        
        toReturn += "\nm_responseId (" + responseIdClass + "): " + responseId ;
        toReturn += "\nm_serverResponseCode (" + serverResponseCodeClass + "): " + serverResponseCode ;
        toReturn += "\nm_requestedServiceCode (" + requestedServiceCodeClass + "): " + requestedServiceCode ;
        toReturn += "\nm_responseDataSize (" + responseDataSizeClass + "): " + responseDataSize ;
        String responseDataCanonicalName = (null != responseDataClass) ? responseDataClass : "anynymous class" ;
        toReturn += "\nm_responseData (" + responseDataCanonicalName + "): " + responseData ;
        
        return toReturn ;
    }
    
    protected void populateFailureDetailsInResponse(Object i_failureDetailsParameter)
    {
        if(null == i_failureDetailsParameter)
            return ;
        
        if(true == Object[].class.isInstance(i_failureDetailsParameter))
        {
            Object[] failureParameters = (Object[])i_failureDetailsParameter ;
            if(true == failureParameters[0] instanceof SSRBasicBackendRequest)
            {
                SSRBasicBackendRequest errorredRequest = (SSRBasicBackendRequest)
                        failureParameters[0] ;
                this.populateResponseFieldsWithCorrespondingRequestFields(errorredRequest);
            }
        }
    }
}
