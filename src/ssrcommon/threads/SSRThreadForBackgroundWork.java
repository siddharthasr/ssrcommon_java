package ssrcommon.threads;

import java.util.GregorianCalendar;
import java.util.HashMap;
import ssrcommon.SSRCommonJavaFunctionalityLibrary;

/**
 * Created on 7/20/2016.
 */
public class SSRThreadForBackgroundWork extends Thread
{
    public HashMap<Object, Object> m_parametersHashMap ;

    public SSRThreadForBackgroundWork() throws Exception
    {
        return ;
    }

    public SSRThreadForBackgroundWork(HashMap<Object, Object> i_parametersHashMap)
            throws Exception
    {
        this.m_parametersHashMap = i_parametersHashMap ;
    }

    public void preRun() throws Exception
    {
        return ;
    }

    public void runMainLogic() throws Exception
    {
        return ;
    }

    public void handleResponse() throws Exception
    {
        return ;
    }
    
    protected void prepareForThreadFlow() throws Exception
    {
        return ;
    }
    
    protected void finishThreadFlowHandling() throws Exception
    {
        return ;
    }

    @Override
    public void run()
    {
        try
        {
            // Prepare thread flow handling
            this.prepareForThreadFlow() ;
            // Application logic related pre-run tasks
            this.preRun() ;
            // Application logic to be run
            this.runMainLogic() ;
            // Application logic related response handling
            this.handleResponse() ;
            // Finish thread flow handling
            this.finishThreadFlowHandling() ;
        }
        catch(Exception e)
        {
            this.handleThreadFlowException("Exception caught at SSRThreadForBackgroundWork.run : \n", e) ;
        }
    }
    
    // Try not to ever make this method to throw exception
    // This has been provided so that in case some special logging is needed
    // when exception happens.
    protected void showExceptionError(String i_errorMessage, Exception i_exceptionToShow)
    {
        this.logMessage(i_errorMessage, i_exceptionToShow) ;
        return ;
    }
    
    // Try not to ever make this method to throw exception
    protected void handleThreadFlowException(String i_errorMessage, Exception i_exceptionToHandle)
    {
        this.showExceptionError(i_errorMessage, i_exceptionToHandle) ;
        return ;
    }
    
    // ASSUMPTION - WE ARE NOT GOING TO EXPECT THAT THE LOGGING MECHANISM MIGHT THROW EXCEPTION
    
    // This method is just some syntactic sugar and nothing more. One could argue
    // whether this is right to exist or not. It gives the impression that there is
    // a reasonable expectation that logging of messages without auxiliary parameter
    // but we will keep it for now, so that we can see in future whether such
    // programming is good practice or not.
    protected void logMessage(String i_logMessage)
    {
        this.logMessage(i_logMessage, null) ;
    }
    
    protected void logMessage(String i_logMessage, Object i_auxiliaryParameter)
    {
        GregorianCalendar logMessageTimeStamp = new GregorianCalendar() ;
        String errorMessagePrefix ;
        try
        {
            errorMessagePrefix = SSRCommonJavaFunctionalityLibrary
                    .getDisplayTimeStampFromCalendar(logMessageTimeStamp) ;
        }
        catch(Exception e)
        {
            errorMessagePrefix = "Exception caught trying to format logmessage timestamp : " 
                    + e.getMessage() ;
        }
        errorMessagePrefix = errorMessagePrefix + " || " 
                + Thread.currentThread().getId() + " || " ;
        System.out.println(errorMessagePrefix + i_logMessage + "\n") ;

        if(null == i_auxiliaryParameter)
            return ;
        
        if(true == i_auxiliaryParameter instanceof Exception)
        {
            ((Exception)i_auxiliaryParameter).printStackTrace(System.out) ;
        }
    }
}
