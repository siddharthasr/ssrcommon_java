package ssrcommon.threads;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created on 12/6/2016.
 * Purpose : This is supposed to be the interface for request classes for backend.
 */
public interface ISSRBackendRequest
{
    public void readRequestFromInputStream(InputStream i_inputStreamToReadFrom) throws Exception ;

    public void writeRequestToOutputStream(OutputStream i_outputStreamToWriteTo) throws Exception ;
}
