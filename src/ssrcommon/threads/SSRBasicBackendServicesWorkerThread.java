package ssrcommon.threads;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.GregorianCalendar;
import ssrcommon.SSRCommonJavaFunctionalityLibrary;

import ssrcommon.database.SSRBasicDBOperationsHandler;

/**
 * Created on 
 *
 * Purpose: This is going to be the basic implementation of a worker thread
 * Changelog :
 * 1) 20170203 :
 * Added logic to the handleClientRequest method, and introduced processClientRequest method
 * so that some exception handling etc. can be moved to this base implementation
 */

public class SSRBasicBackendServicesWorkerThread extends SSRThreadForBackgroundWork
        implements ISSRBackendServiceWorkerThread
{	
    public static final String DUMMY_REQUEST = "DUMMY_REQUEST" ;
    public static final String RESPONSE_OK = "RESPONSE_OKAY" ;
    public static final String RESPONSE_FAILURE = "RESPONSE_FAILURE" ;
    
    protected Socket m_serviceSocket;
    protected InputStream m_inputStreamFromSocket;
    
    // The request and response data members are although not used in the basic
    // implementation, to ensure flexibility, they are still provided because
    // they are very frequently the mode of implementing the ssrcommon client-server
    // architecture.
    protected ISSRBackendRequest m_clientRequest;
    protected ISSRBackendResponse m_responseToClient;

    public SSRBasicBackendServicesWorkerThread(Socket i_socketToGiveServiceTo)
            throws Exception
    {
            this.m_serviceSocket = i_socketToGiveServiceTo ;
    }
	
    @Override
    public void run() 
    {
        try
        {
            this.logMessage("SSRBasicBackendServicesWorkerThread version 1.1", null) ;
            //this.m_inputStreamFromSocket = this.m_serviceSocket.getInputStream() ;

            this.handleClientRequest() ;

            System.out.println("Client request was handled.") ;
        }
        catch (Exception e)
        {
                // TODO : Do proper exception handling here
                System.out.println("Exception caught 2 : " + e.getMessage()) ;
                e.printStackTrace(System.out) ;
                if(null != this.m_serviceSocket && true == this.m_serviceSocket.isConnected())
                {
                    if(null == this.m_responseToClient)
                    {
                        try
                        {
                            this.getResponseObjectInstance() ;
                            this.m_responseToClient.populateWithFailureResponseData(
                                SSRBasicBackendResponse.GENERAL_BACKEND_ERROR, null) ;
                            this.m_responseToClient.writeResponseToOutputStream(
                                    this.m_serviceSocket.getOutputStream()) ;
                        }
                        catch(Exception f)
                        {
                            System.out.println("Exception caught while trying to handle exception 2.1a") ;
                            f.printStackTrace(System.out) ;
                            return ;
                        }
                    }
                }
                return ;
        }
    }
    
    // Derived classes can override this method to take complete control of
    // client request handling, or they can override the processClientRequest
    // method, to handle only the application logica part, if they are using
    // the standard SSRBasicBackendRequest and response classes and don't need
    // any special flow.
    protected void handleClientRequest() throws Exception
    {
        System.out.println("Entering SSRBasicBackendServicesWorker.handleClientRequest") ;
        this.logMessage("SSRBasicBackendServicesWorker version 1.1 (20170203)") ;
        this.getRequestObjectInstance() ;
        this.logMessage("Starting to read the requestData : ", null) ;
        this.m_clientRequest.readRequestFromInputStream(this.m_serviceSocket.getInputStream()) ;
        this.logMessage("Finished reading requestData.", null) ;
        
        this.logMessage("Client request data : " + this.m_clientRequest.toString(), null) ;
        
        try
        {
            this.processClientRequest() ;
        }
        catch(Exception e)
        {
            System.out.println("Exception in SSRBasicBackendServicesWorkerThread.handleClientRequest :\n") ;
            e.printStackTrace(System.out) ;
            if(null != this.m_serviceSocket && true == this.m_serviceSocket.isConnected())
            {
                SSRBasicBackendResponse errorResponse = new SSRBasicBackendResponse() ;
                errorResponse.m_responseId = ((SSRBasicBackendRequest)this
                        .m_clientRequest).m_requestId ;
                errorResponse.m_serverResponseCode = SSRBasicBackendServicesWorkerThread.RESPONSE_FAILURE ;
                errorResponse.m_requestedServiceCode = ((SSRBasicBackendRequest)this
                        .m_clientRequest).m_serviceRequestCode ;
                errorResponse.m_responseDataSize = 0 ;
                errorResponse.m_responseData = errorResponse.m_serverResponseCode ;
           
                errorResponse.writeResponseToOutputStream(this.m_serviceSocket.getOutputStream()) ;
                this.m_serviceSocket.close() ;
                
                System.out.println("Error response sent to client. Client socket closed") ;
                throw e ;
            }
            else
            {
                System.out.println("Client service socket already closed.") ;
                throw e ;
            }
        }
        
        System.out.println("Response to client : " + this.m_responseToClient.toString()) ;
        return ;
    }
    
    // Derived classes can override this method to take control of only application
    // logic part of client request handling, or they can override the handleClientRequest
    // method, take complete control of the flow.
    protected void processClientRequest() throws Exception
    {
        this.logMessage("Entering SSRBasicBackendServicesWorker.processClientRequest") ;
        return ;
    }
    
    // This method is being provided, but it is not called in the SSRCommon server
    // framework because doing that would have restricted the first input that is sent
    // over the socket, to be a request object. But still it is provided, because
    // in most cases it probably willl be and in such cases, the overridden 
    // handleClientRequest could just call this method to get the request object
    protected void getRequestObjectInstance() throws Exception
    {
        System.out.println("Entering SSRBasicBackendServicesWorker.getRequestObjectInstance") ;
        this.m_clientRequest = new SSRBasicBackendRequest() ;
    }
    
    // Please refer to the documentation of the method getRequestObjectInstance for
    // insight into this method.
    protected void getResponseObjectInstance() throws Exception
    {
        System.out.println("Entering SSRBasicBackendServicesWorker.getResponseObjectInstance") ;
        this.m_responseToClient = new SSRBasicBackendResponse() ;
    }

    protected Integer handleRunCustomDmlRequest(String i_customDmlStatementToRun)
            throws Exception
    {
        System.out.println("Entering SSRBasicBackendServicesWorker.handleRunCustomDmlRequest") ;
        SSRBasicDBOperationsHandler dbHandler = new SSRBasicDBOperationsHandler() ;
        Integer updatedRows = new Integer(dbHandler
                    .runCustomDml(i_customDmlStatementToRun)) ;
        System.out.println("DML query was updated. Rows affected : " 
                    + updatedRows.toString()) ;
        return updatedRows ;
    }

    protected Object[][] handleRunCustomQueryRequest(String i_customQueryToRun)
            throws Exception
    {
        System.out.println("Entering SSRBasicBackendServicesWorker.handleRunCustomQueryRequest") ;
        SSRBasicDBOperationsHandler dbHandler = new SSRBasicDBOperationsHandler() ;
        Object[][] result = dbHandler.runCustomQuery(i_customQueryToRun) ;
        System.out.println("Got the response.") ;
        return result ;
    }
    
    protected void writeResponseToSocket() throws Exception
    {
        System.out.println("Entering SSRBasicBackendServicesWorker.writeResponseToSocket") ;
        OutputStream responseOutputStream = this.m_serviceSocket.getOutputStream();
        this.m_responseToClient.writeResponseToOutputStream(responseOutputStream);
    }
    
    // This method is moving of frequently used code towards base class.
    protected void prepareStandardSuccessfulResponse() throws Exception
    {
        this.getResponseObjectInstance() ;
        SSRBasicBackendResponse response = (SSRBasicBackendResponse)this.m_responseToClient ;
        response.populateResponseFieldsWithCorrespondingRequestFields(
                (SSRBasicBackendRequest)this.m_clientRequest);
        response.m_serverResponseCode = SSRBasicBackendResponse.RESPONSE_OK ;
    }
    
    // This method is moving of frequently used code towards base class.
    protected void prepareStandardFailureResponse() throws Exception
    {
        this.getResponseObjectInstance() ;
        SSRBasicBackendResponse response = (SSRBasicBackendResponse)this.m_responseToClient ;
        response.populateResponseFieldsWithCorrespondingRequestFields(
                (SSRBasicBackendRequest)this.m_clientRequest);
        response.m_serverResponseCode = SSRBasicBackendResponse.RESPONSE_FAILURE ;
    }
}
