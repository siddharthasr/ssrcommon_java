package ssrcommon;

public class SSRSimpleMutex 
{
    protected int m_entrantsToContentiousCode ;
    protected long m_ownerThreadId ;

    public SSRSimpleMutex() throws Exception
    {
        this.m_entrantsToContentiousCode = 0 ;
        this.m_ownerThreadId = 0 ;
    }

    public long getOwnerThreadId() throws Exception
    {
        return this.m_ownerThreadId ;
    }
    
    public Boolean acquireNoWait() throws Exception
    {
        Boolean toReturn = false ;
        
        long currentThreadId = Thread.currentThread().getId() ;
        if (currentThreadId == this.m_ownerThreadId)
        {
            // This mutex is already locked by the thread which called for acquire
            // TODO : Think and decide whether this is an okay situation or an exception situation
            throw new Exception("Thread '" + Thread.currentThread().getId() + "' is trying to acquire the same mutex twice.") ;
        }
        
        if(0 == this.m_entrantsToContentiousCode)
        {
            this.m_entrantsToContentiousCode++ ;
            if(1 == this.m_entrantsToContentiousCode)
            {
                this.m_ownerThreadId = currentThreadId ;
                toReturn = true ;
            }
            else
                this.m_entrantsToContentiousCode-- ;
        }
        return toReturn ;
    }

    public void acquire(int i_spinLockWaitInMilliSeconds) throws Exception
    {
        /*long currentThreadId = Thread.currentThread().getId() ;
        if (currentThreadId == this.m_ownerThreadId)
        {
            // This mutex is already locked by the thread which called for acquire
            // TODO : Think and decide whether this is an okay situation or an exception situation
            throw new Exception("Thread '" + Thread.currentThread().getId() + "' is trying to acquire the same mutex twice.") ;
        }*/

        /*while(true)*/
        while(true != this.acquireNoWait())
        {
            /*if(0 == this.m_entrantsToContentiousCode)
            {
                this.m_entrantsToContentiousCode++ ;
                if(1 == this.m_entrantsToContentiousCode)
                {
                    this.m_ownerThreadId = currentThreadId ;
                    break ;
                }
                else
                    this.m_entrantsToContentiousCode-- ;
            }*/
            Thread.sleep(i_spinLockWaitInMilliSeconds) ;
        }
    }

    public void release() throws Exception
    {
        long currentThreadId = Thread.currentThread().getId() ;
        if(currentThreadId != this.m_ownerThreadId)
        {
            // TODO : The thread that is asking for release is not the owner. What to do now?
            return ;
        }
        this.m_ownerThreadId = 0 ;
        this.m_entrantsToContentiousCode-- ;
    }

    public boolean checkMutexFree() throws Exception
    {
        return (0 == this.m_entrantsToContentiousCode) ;
    }

    public void handOverMutexToAnotherThread(long i_mutexOwnershipReceiverThreadId) 
    		throws Exception
    {
        if(Thread.currentThread().getId() != this.m_ownerThreadId)
            return ;

        this.m_ownerThreadId = i_mutexOwnershipReceiverThreadId ;
    }
}
