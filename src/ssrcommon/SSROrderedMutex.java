package ssrcommon;

import java.util.Vector;

public class SSROrderedMutex extends SSRSimpleMutex
{
    protected SSRSimpleMutex m_queueProtectMutex ;
    protected Vector<Long> m_threadOrderQueue ;

    public SSROrderedMutex() throws Exception
    {
        super() ;
        this.m_threadOrderQueue = new Vector<Long>() ;
        this.m_queueProtectMutex = new SSRSimpleMutex() ;
    }

    public void insertThreadInQueue(Long i_threadToInsertInQueue) throws Exception
    {
        this.m_queueProtectMutex.acquire(100);

        if(0 == this.m_ownerThreadId)
            this.m_ownerThreadId = i_threadToInsertInQueue ;
        else
            this.m_threadOrderQueue.add(new Long(i_threadToInsertInQueue)) ;

        this.m_queueProtectMutex.release() ;
    }

    protected void popThreadsInQueue() throws Exception
    {
        this.m_queueProtectMutex.acquire(100);

        if(0 == this.m_threadOrderQueue.size())
            this.m_ownerThreadId = 0 ;
        else
        {
            this.m_ownerThreadId = this.m_threadOrderQueue.firstElement() ;
            this.m_threadOrderQueue.remove(0) ;
        }
        this.m_queueProtectMutex.release() ;
    }

    public void acquire(int i_spinLockWaitInMilliSeconds) throws Exception
    {
        long currentThreadId = Thread.currentThread().getId() ;
        if (currentThreadId == this.m_ownerThreadId)
        {
            // The current thread is set as the owner / next-operation-in-queue.
            return ;
        }

        while(true)
        {
            // Keep spinning in this loop, until you become the owner of this mutex
            if(currentThreadId == this.m_ownerThreadId)
                return ;

            Thread.sleep(i_spinLockWaitInMilliSeconds) ;
        }
    }

    public void release() throws Exception
    {
        long currentThreadId = Thread.currentThread().getId() ;
        if(currentThreadId != this.m_ownerThreadId)
        {
            // TODO : The thread that is asking for release is not the owner. What to do now?
            //        This should be an exception scenario.
            return ;
        }
        
        this.popThreadsInQueue() ;
    }
}
