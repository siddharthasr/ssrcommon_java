package ssrcommon;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Vector;

/****************************************************************************
 * It is important to not issue multiple queue and dequeue requests.
*****************************************************************************/
public class SSROrderedReadWriteMutex 
{
    public static enum ResourceOperation {READ, WRITE} ;
    protected static enum MutexOperation {QUEUE, DEQUEUE, CHECKTHREADREADYFOROPERATION} ;

    protected Vector<Map.Entry<ResourceOperation, Vector<Long> > > m_orderedReadWriteQueue ;

    protected Integer m_lastWriteRequestIndex ;
    protected Integer m_lastReadRequestIndex ;
	
    public SSROrderedReadWriteMutex()
    {
        this.m_orderedReadWriteQueue = 
            new Vector<Map.Entry<ResourceOperation, Vector<Long> > >() ;
        this.m_lastWriteRequestIndex = -1 ;
        this.m_lastReadRequestIndex = -1 ;
    }
	
    public void acquire(ResourceOperation i_resourceOperation, long i_waitDuration) 
        throws Exception
    {
        this.performMutexOperation(Thread.currentThread().getId(), 
            SSROrderedReadWriteMutex.MutexOperation.QUEUE, i_resourceOperation) ;
        while(true)
        {
            Boolean checkResult = this.performMutexOperation(Thread.currentThread().getId(),
                SSROrderedReadWriteMutex.MutexOperation.CHECKTHREADREADYFOROPERATION,
                i_resourceOperation) ;
            if(false == checkResult)
                Thread.sleep(i_waitDuration) ;
            else
                return ;
        }
    }
	
    public void release() throws Exception
    {
        this.performMutexOperation(Thread.currentThread().getId(), 
            MutexOperation.DEQUEUE, null) ;
    }
	
    protected synchronized Boolean performMutexOperation(long i_threadId, 
            MutexOperation i_mutexOperation,
            ResourceOperation i_resourceOperation) throws Exception
    {
        Boolean toReturn = true ;
        switch(i_mutexOperation)
        {
            case QUEUE:
            {
                this.queueRequest(i_threadId, i_resourceOperation) ;
                break ;
            }

            case DEQUEUE:
            {
                this.deQueueRequest(i_threadId) ;
                break ;
            }

            case CHECKTHREADREADYFOROPERATION:
            {
                toReturn = this.checkThreadReadyForOperation(i_threadId, 
                                i_resourceOperation) ;
                break ;
            }

            default :
            {
                throw new Exception("Invalid mutex operation requested to "
                                + "SSROrderedReadWriteMutex.performMutexOperation") ;
            }
        }
        return toReturn ;
    }
	
    private void queueRequest(long i_threadId, ResourceOperation i_resourceOperation) 
            throws Exception
    {
        if(0 == this.m_orderedReadWriteQueue.size())
        {
            this.pushNewRequest(i_threadId, i_resourceOperation) ;
            return ;
        }

        Map.Entry<ResourceOperation, Vector<Long> > lastQueueElement = 
            this.m_orderedReadWriteQueue.lastElement() ;

        if(SSROrderedReadWriteMutex.ResourceOperation.WRITE == i_resourceOperation
                || SSROrderedReadWriteMutex.ResourceOperation.WRITE == lastQueueElement.getKey())
        {
            this.pushNewRequest(i_threadId, i_resourceOperation) ;
            return ;
        }

        if(SSROrderedReadWriteMutex.ResourceOperation.READ == lastQueueElement
                .getKey())
        {
            Vector<Long> queuedThreadsVector = lastQueueElement.getValue() ;
            queuedThreadsVector.add(i_threadId) ;
        }

        throw new Exception("Logical error at SSROrderedReadWriteMutex.queueRequest") ;
    }
	
    private void deQueueRequest(long i_threadId) throws Exception
    {
        Map.Entry<ResourceOperation, Vector<Long> > firstQueueEntry =
                this.m_orderedReadWriteQueue.firstElement() ;

        if(firstQueueEntry.getValue().contains(i_threadId))
        {
            if(SSROrderedReadWriteMutex.ResourceOperation.WRITE == firstQueueEntry.getKey())
            {
                this.removeOneRequest() ;
                return ;
            }
            else
            if(SSROrderedReadWriteMutex.ResourceOperation.READ == firstQueueEntry.getKey())
            {
                Vector<Long> threadsVector = firstQueueEntry.getValue() ;
                threadsVector.remove(new Long(i_threadId)) ;
                if(0 == threadsVector.size())
                        this.removeOneRequest() ;
                return ;
            }
        }

        throw new Exception(new String("Dequeue request made when it was not turn of ")
                + "the thread to use the ordered mutex") ;
    }

    private void pushNewRequest(long i_threadId, SSROrderedReadWriteMutex
                    .ResourceOperation i_resourceOperation)
    {
        Vector<Long> newRequest = new Vector<Long>() ;
        newRequest.add(i_threadId) ;
        Map.Entry<ResourceOperation, Vector<Long> > oneOrderedEntry =
                new AbstractMap.SimpleImmutableEntry<ResourceOperation, Vector<Long> >(
                        i_resourceOperation, newRequest) ;
        this.m_orderedReadWriteQueue.add(oneOrderedEntry) ;
    }

    private void removeOneRequest()
    {
    	this.m_orderedReadWriteQueue.remove(0) ;
    }

    private Boolean checkThreadReadyForOperation(long i_threadId, 
    		ResourceOperation i_resourceOperation) throws Exception
    {
    	Boolean toReturn = true ;
    	Map.Entry<ResourceOperation, Vector<Long> > firstEntry =
                this.m_orderedReadWriteQueue.firstElement() ;
    	if(firstEntry.getKey() != i_resourceOperation)
    		return false ;
    	toReturn = firstEntry.getValue().contains(new Long(i_threadId)) ;
    	return toReturn ;
    }
}
