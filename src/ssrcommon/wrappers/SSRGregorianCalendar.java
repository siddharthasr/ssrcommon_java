/*
 * Purpose : This is serializable and timezone adjustable version of the GregorianCalendar.
 */
package ssrcommon.wrappers;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Vector;

/**
 *
 * @author LENOVO
 */
public class SSRGregorianCalendar extends GregorianCalendar implements Serializable
{
    protected String m_timeZoneID ;
    protected Long m_timeInMilliseconds ;
    
    public static void convertToLocalTimeZone(SSRGregorianCalendar io_ssrCalendarToConvert)
    {
        GregorianCalendar currentTimestampLocal = new GregorianCalendar() ;
        int currentLocalTimeRawOffset = currentTimestampLocal.getTimeZone()
                .getOffset(currentTimestampLocal.getTimeInMillis()) ;
        int inputTimestampOffset = TimeZone.getTimeZone(io_ssrCalendarToConvert.m_timeZoneID)
                        .getOffset(io_ssrCalendarToConvert.m_timeInMilliseconds) ;

        io_ssrCalendarToConvert.setTimeZone(currentTimestampLocal.getTimeZone()) ;
        io_ssrCalendarToConvert.m_timeZoneID = currentTimestampLocal.getTimeZone().getID() ;
        io_ssrCalendarToConvert.setTimeInMillis(io_ssrCalendarToConvert.m_timeInMilliseconds 
                - inputTimestampOffset + currentLocalTimeRawOffset) ;
        io_ssrCalendarToConvert.m_timeInMilliseconds = io_ssrCalendarToConvert.getTimeInMillis() ;
    }
    
    private void writeObject(java.io.ObjectOutputStream out) throws IOException
    {
        out.writeObject(this.m_timeZoneID) ;
        out.writeObject(this.m_timeInMilliseconds) ;
    }

    private void readObject(java.io.ObjectInputStream in) 
            throws IOException, ClassNotFoundException
    {
        this.m_timeZoneID = (String)in.readObject() ;
        this.m_timeInMilliseconds = (Long)in.readObject() ;
        SSRGregorianCalendar.convertToLocalTimeZone(this) ;
    }

    private void readObjectNoData() throws ObjectStreamException
    {
        throw new NotSerializableException("SSRGregorianCalendar") ;
    }
}
