/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon.design;

import ssrcommon.SSROrderedMutexProtectedHashMap;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 1/16/2017.
 * Purpose : This class will manage the "key controlled callback" design pattern. This class does not
 *           enforce "type" checking. Only that callers which have the right key and callback identifier,
 *           are able to call the methods of the key controlled callback interface.
 * Changelog:
 * 1) 20170516 -
 * Moved the code from Android based SSRCommonLibrary to java based SSRCommon library
 */
public class SSRKeyControlledCallbackGateKeeper
{
    protected static SSRKeyControlledCallbackGateKeeper ms_uniqueInstance ;

    protected SSROrderedMutexProtectedHashMap<String, ISSRKeyControlledCallbackInterface> m_registeredCallbacks ;

    public static SSRKeyControlledCallbackGateKeeper getInstance() throws Exception
    {
        SSRKeyControlledCallbackGateKeeper toReturn = null ;
        if(null == SSRKeyControlledCallbackGateKeeper.ms_uniqueInstance)
        {
            SSRKeyControlledCallbackGateKeeper.createUniqueInstance() ;
            toReturn = SSRKeyControlledCallbackGateKeeper.ms_uniqueInstance ;
        }
        else
        {
            toReturn = SSRKeyControlledCallbackGateKeeper.ms_uniqueInstance ;
        }
        return toReturn ;
    }

    private static synchronized void createUniqueInstance() throws Exception
    {
        if(null == SSRKeyControlledCallbackGateKeeper.ms_uniqueInstance)
        {
            SSRKeyControlledCallbackGateKeeper.ms_uniqueInstance = new SSRKeyControlledCallbackGateKeeper() ;
        }
    }

    public void registerCallback(String i_callbackIdentifier, ISSRKeyControlledCallbackInterface i_callbackObject)
        throws Exception
    {
        // This is important, because the callback interface is not static, and hence, it is necessary
        // to maintain the objects that are being used to implement those callbacks, in order to maintain
        // the context.
        if(true == this.m_registeredCallbacks.containsKey(i_callbackIdentifier))
            throw new SSRBasicException("Callback idenfier is already registered") ;

        this.m_registeredCallbacks.put(i_callbackIdentifier, i_callbackObject) ;
    }

    public Boolean checkCallbackRegistered(String i_callbackIdentifier) throws Exception
    {
        return this.m_registeredCallbacks.containsKey(i_callbackIdentifier) ;
    }

    // This method will return null if the callback is not registered at the time of calling
    public ISSRKeyControlledCallbackInterface deRegisterCallback(String i_callbackIdentifier) throws Exception
    {
        ISSRKeyControlledCallbackInterface toReturn = this.m_registeredCallbacks.remove(i_callbackIdentifier) ;
        return toReturn ;
    }

    public Object performCallback(String i_callbackIdentifier, Object i_key, Object i_inputParameters)
        throws Exception
    {
        ISSRKeyControlledCallbackInterface callbackObject = this.m_registeredCallbacks.get(i_callbackIdentifier) ;
        if(null == callbackObject)
            throw new SSRBasicException("Invalid attempt to perform callback on non-existent "
                    + "callback identifier : " + i_callbackIdentifier) ;

        if(false == callbackObject.checkKeyFits(i_key))
            throw new SSRBasicException("Invalid attempt to perform callback using invalid key "
                    + "key.toString : " + i_key.toString()) ;

        Object toReturn = callbackObject.executeCallback(i_key, i_inputParameters) ;
        return toReturn ;
    }

    protected SSRKeyControlledCallbackGateKeeper() throws Exception
    {
        this.m_registeredCallbacks = new SSROrderedMutexProtectedHashMap<String, ISSRKeyControlledCallbackInterface>() ;
    }
}