/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon.design;

/**
 * Created on 1/16/2017.
 * Purpose : This interface will define the behaviour of the callback-able objects. It is the duty of the
 *           callback object to recognize the key that it had previously given around.
 * Changelog:
 * 1) 20170516 -
 * Moved the code from Android based SSRCommonLibrary to java based SSRCommon library
 */
public interface ISSRKeyControlledCallbackInterface
{
    // This method will check and confirm whether the input key "fits" or not. ie. whether the callback
    // object is going to accept access to itself using that key or not.
    public Boolean checkKeyFits(Object i_key) throws Exception ;

    // This method is to execute the callback interface. One of its entry points.
    public Object executeCallback(Object i_key, Object i_inputParameters) throws Exception ;
}