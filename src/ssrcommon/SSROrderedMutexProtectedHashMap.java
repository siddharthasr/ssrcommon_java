/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import ssrcommon.exceptions.SSRBasicException;

/**
 *
 * Purpose : This is a template that will wrap a HashMap and allow access to it
 *           via ordered mutex.
 * @param <K> - Key to use in hashmap
 * @param <V> - Value to use in hashmap
 */
public class SSROrderedMutexProtectedHashMap <K, V>
{
    public HashMap<K, V> m_wrappedHashMap ;
    public int m_mutexWaitDuration ;
    protected SSROrderedReadWriteMutex m_orderedReadWriteMutex ;
    
    protected class MutexProtectedOperation<T>
    {
        public T m_operationResult ;
        
        public void executeOperation() throws Exception 
        {
            throw new SSRBasicException("Implementation not given for " 
                    + "MutexProtectedOperation<T>.executeOperation.");
        }
    }
    
    public SSROrderedMutexProtectedHashMap() throws Exception
    {
        this.m_wrappedHashMap = new HashMap<K, V>() ;
        this.m_orderedReadWriteMutex = new SSROrderedReadWriteMutex() ;
        this.m_mutexWaitDuration = 100 ;
    }
    
    protected <R> void performMutexProtectedOperation(MutexProtectedOperation<R> i_operation,
            SSROrderedReadWriteMutex.ResourceOperation i_resourceOperation)
            throws Exception
    {
        Boolean mutexAcquired = false ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.READ, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;

            i_operation.executeOperation() ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
    }
    
    public boolean containsKey(K i_keyToCheck) throws Exception
    {
        Boolean mutexAcquired = false ;
        boolean toReturn = false ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.READ, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            toReturn = this.m_wrappedHashMap.containsKey(i_keyToCheck) ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
        return toReturn ;
    }
    
    public V get(K i_keyToGet) throws Exception
    {
        Boolean mutexAcquired = false ;
        V toReturn ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.READ, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            toReturn = this.m_wrappedHashMap.get(i_keyToGet) ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
        return toReturn ;
    }
    
    public void put(K i_keyToPut, V i_valueToPut) throws Exception
    {
        Boolean mutexAcquired = false ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.WRITE, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            this.m_wrappedHashMap.put(i_keyToPut, i_valueToPut) ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
    }
    
    public V remove(K i_keyToRemove) throws Exception
    {
        /*Boolean mutexAcquired = false ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.WRITE, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            this.m_wrappedHashMap.remove(i_keyToRemove) ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }*/
        final K keyToRemove = i_keyToRemove ;
        MutexProtectedOperation<V> oneOperation = new MutexProtectedOperation<V>()
        {
            @Override
            public void executeOperation() throws Exception
            {
                this.m_operationResult = m_wrappedHashMap.remove(keyToRemove) ;
            }
        } ;
        this.<V>performMutexProtectedOperation(oneOperation, 
                SSROrderedReadWriteMutex.ResourceOperation.WRITE) ;
        return oneOperation.m_operationResult ;
    }
    
    public int size() throws Exception
    {
        Boolean mutexAcquired = false ;
        int toReturn = -1 ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.READ, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            toReturn = this.m_wrappedHashMap.size() ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
        return toReturn ;
    }
    
    public Collection<V> values() throws Exception
    {
        Boolean mutexAcquired = false ;
        Collection<V> toReturn ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.READ, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            toReturn = this.m_wrappedHashMap.values() ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
        return toReturn ;
    }
    
    public boolean containsValue(V i_valueToCheck) throws Exception
    {
        Boolean mutexAcquired = false ;
        boolean toReturn = false ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.READ, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            toReturn = this.m_wrappedHashMap.containsValue(i_valueToCheck) ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
        return toReturn ;
    }
    
    public Set<K> keySet() throws Exception
    {
        MutexProtectedOperation<Set<K>> oneOperation = new MutexProtectedOperation<Set<K>>()
        {
            @Override
            public void executeOperation() throws Exception
            {
                this.m_operationResult = m_wrappedHashMap.keySet() ;
            }
        } ;
        this.<Set<K>>performMutexProtectedOperation(oneOperation, 
                SSROrderedReadWriteMutex.ResourceOperation.READ) ;
        return oneOperation.m_operationResult ;
    }
    
    public Set<Map.Entry<K,V>> entrySet() throws Exception
    {
        MutexProtectedOperation<Set<Map.Entry<K,V>>> oneOperation = new MutexProtectedOperation<Set<Map.Entry<K,V>>>()
        {
            @Override
            public void executeOperation() throws Exception
            {
                this.m_operationResult = m_wrappedHashMap.entrySet() ;
            }
        } ;
        this.<Set<Map.Entry<K,V>>>performMutexProtectedOperation(oneOperation, 
                SSROrderedReadWriteMutex.ResourceOperation.READ) ;
        return oneOperation.m_operationResult ;
    }
    
    public void clear() throws Exception
    {
        Boolean mutexAcquired = false ;
        try
        {
            this.m_orderedReadWriteMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.WRITE, 
                    this.m_mutexWaitDuration) ;
            mutexAcquired = true ;
            this.m_wrappedHashMap.clear() ;
        }
        catch(Exception e)
        {
            throw e ;
        }
        finally
        {
            if(true == mutexAcquired)
                this.m_orderedReadWriteMutex.release() ;
        }
    }
}
