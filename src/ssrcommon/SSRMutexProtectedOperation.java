/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon;

import ssrcommon.exceptions.SSRBasicException;

/**
 *
 * @author LENOVO
 */
public abstract class SSRMutexProtectedOperation <T, R>
{
    public R m_operationResult ;
    public Object[] m_operationParameters ;
    
    public abstract void executeOperation(T i_objectForOperation) throws Exception ;
}
