/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon.exceptions;

/**
 *
 * @author LENOVO
 */
public class SSRBasicException extends Exception implements ISSRExceptionInterface
{
    protected Object m_exceptionDetailObject ;
    
    public SSRBasicException()
    {
        super() ;
    }
    
    public SSRBasicException(String message)
    {
        super(message) ;
    }
    
    public SSRBasicException(String message, Throwable cause)
    {
        super(message, cause) ;
    }
    
    public SSRBasicException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace) ;
    }
    
    public SSRBasicException(Throwable cause)
    {
        super(cause) ;
    }

    public Object getExceptionDetailObject() 
    {
        return this.m_exceptionDetailObject;
    }

    public void setExceptionDetailObject(Object i_exceptionDetailObject) 
    {
        this.m_exceptionDetailObject = i_exceptionDetailObject;
    }
    
    @Override
    public Boolean checkHandled()
    {
        return false ;
    }
}
