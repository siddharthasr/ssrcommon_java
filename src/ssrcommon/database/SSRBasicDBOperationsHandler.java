package ssrcommon.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.serial.SerialBlob;

public class SSRBasicDBOperationsHandler 
{

        protected String m_driverClassName ;
        protected String m_connectionString ;
        protected String m_user ;
        protected String m_password ;
	protected Connection m_connection ;
	protected Statement m_statement ;
	
	public SSRBasicDBOperationsHandler() throws Exception
	{
		this(new String("com.mysql.jdbc.Driver"),
                        new String("jdbc:mysql://159.203.132.191:3306/aegisdb"), 
                        new String("aegisuser"), new String("aegisuser")) ;
	}
        
        public SSRBasicDBOperationsHandler(String i_driverClassName, String i_connectionString,
                String i_user, String i_password) throws Exception
        {
            this.m_driverClassName = i_driverClassName ;
            this.m_connectionString = i_connectionString ;
            this.m_user = i_user ;
            this.m_password = i_password ;
            this.createConnection() ;
            this.m_statement = this.m_connection.createStatement() ;
        }
	
	protected void createConnection() throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.jdbc.Driver");  
		  
		/*this.m_connection = DriverManager.getConnection(  
		"jdbc:mysql://127.0.0.1:3306/aegisdb","aegisuser","aegisuser");
		this.m_connection = DriverManager.getConnection(
                        "jdbc:mysql://159.203.132.191:3306/aegisdb","aegisuser","aegisuser");*/
                                        
                this.m_connection = DriverManager.getConnection(
                        this.m_connectionString, this.m_user, this.m_password) ;
	}
	
	public Object[][] runCustomQuery(String i_queryToRun)
	{
		Object[][] ssrResultSet = new Object[1][1];
		try
		{
			ResultSet queryResult = this.m_statement.executeQuery(i_queryToRun) ;
			
			// By replacing this next call and uncommenting the block below, the original
			// code will be re-instated
			ssrResultSet = this.convertResultSetToSSRResultSet(queryResult) ;
		}
		catch (Exception e)
		{
			// TODO : Do proper exception handling here
			System.out.println("Exception caught 3 : " + e.getMessage()) ;
			e.printStackTrace(System.out) ;
		}
		return ssrResultSet ;
	}
	
	public Integer runCustomDml(String i_queryToRun)
	{
		Integer numberOfRowsAffected = null ;
		try
		{
			numberOfRowsAffected = new Integer(this.m_statement.executeUpdate(i_queryToRun)) ;
			// TODO : There should be a proper commit mechanism. Right now autocommit is
			//        true (default behaviour).
			
			//this.m_connection.commit();
		}
		catch (Exception e)
		{
			// TODO : Do proper exception handling here
			System.out.println("Exception caught 4.1 : " + e.getMessage()) ;
			e.printStackTrace(System.out) ;
		}
		return numberOfRowsAffected ;
	}

	// This method should be deprecated eventually, and its overloaded method used.
	public Integer runCustomDmlPreparedStatement(String i_queryToRun, 
			Object[] i_parametersForQuery)
	{
		Integer numberOfRowsAffected = null ;
		
		try
		{
			PreparedStatement preparedStatementToUse = this.m_connection
					.prepareStatement(i_queryToRun) ;
			// Right now we are assuming that all the input parameters are blobs
			// to be set as byte array byte[]
			for(int parametersLooper = 0 ; parametersLooper < i_parametersForQuery.length ;
					parametersLooper++)
			{
				byte[] oneByteArrayToMakeBlob = (byte[])i_parametersForQuery[parametersLooper] ;
				SerialBlob blobToUse = new SerialBlob(oneByteArrayToMakeBlob) ;
				if(oneByteArrayToMakeBlob.length != blobToUse.length())
				{
					// Seems like the SerialBlob object constructed above didn't
					// instantiate properly and eat up all the bytes fed to it.
					throw new Exception("SerialBlob object didn't eat all the input bytes.") ;
				}
				// It seems like for setting parameters, the counting starts from 1
				preparedStatementToUse.setBlob(parametersLooper + 1, blobToUse) ;
			}
			numberOfRowsAffected = new Integer(preparedStatementToUse
					.executeUpdate()) ;
			// TODO : There should be a proper commit mechanism. Right now autocommit is
			//        true (default behaviour).
			
			//this.m_connection.commit();
		}
		catch (Exception e)
		{
			// TODO : Do proper exception handling here
			System.out.println("Exception caught 4.1 : " + e.getMessage()) ;
			System.out.println("Query statement is : \n" + i_queryToRun) ;
			e.printStackTrace(System.out) ;
		}
		return numberOfRowsAffected ;
	}

	// This method is generic prepared statement update method
	public Integer runCustomPreparedDmlStatement(String i_queryToRun,
			Object[][] i_bindParams) throws Exception
	{
            System.out.println("Entering SSRBasicDBOperationsHandler.runCustomPreparedDmlStatement") ;
            System.out.println("Query statement is : \n" + i_queryToRun) ;
            Integer dmlAffectedRowCountToReturn = null ;

            PreparedStatement preparedStatementToUse = this.m_connection
                    .prepareStatement(i_queryToRun);
            for (int parametersLooper = 0; 
                    parametersLooper < i_bindParams[0].length;
                    parametersLooper++) 
            {
                if (null != i_bindParams[1][parametersLooper]) 
                {
                    System.out.println("Bound parameter ["
                            + parametersLooper + "] value : "
                            + i_bindParams[1][parametersLooper].toString());
                    preparedStatementToUse.setObject(parametersLooper + 1,
                            i_bindParams[1][parametersLooper],
                            (Integer) i_bindParams[0][parametersLooper]);
                } 
                else 
                {
                    System.out.println("Bound parameter ["
                            + parametersLooper + "] value : null");
                    preparedStatementToUse.setNull(parametersLooper + 1,
                            (Integer) i_bindParams[0][parametersLooper]);
                }
            }
            dmlAffectedRowCountToReturn = preparedStatementToUse.executeUpdate();

            System.out.println("Prepared DML update has updated " 
                    + dmlAffectedRowCountToReturn + " rows") ;
            return dmlAffectedRowCountToReturn ;
	}
	
	// The bindData parameter is array of [rows][columns] where rows is always 2, and the
	// first row is the type of the bind parameter.
    public Object[][] runCustomPreparedFetchQuery(String i_queryToRun,
    		Object[][] i_bindParams)
    {
    	System.out.println("6.0.1 Query statement is : \n" + i_queryToRun) ;
    	
    	Object[][] resultToReturn = null ;
    	
    	try
    	{
    		PreparedStatement preparedStatementToUse = this.m_connection
					.prepareStatement(i_queryToRun) ;
    		for(int parametersLooper = 0 ; parametersLooper < i_bindParams[0].length ;
					parametersLooper++)
    		{
    			if(null != i_bindParams[1][parametersLooper])
    			{
    				System.out.println("Bound parameter <"
    						+ parametersLooper + "> value : " 
    						+ i_bindParams[1][parametersLooper].toString()) ;
    				preparedStatementToUse.setObject(parametersLooper + 1, 
    					i_bindParams[1][parametersLooper], 
    					(Integer)i_bindParams[0][parametersLooper]) ;
    			}
    			else
    			{
    				System.out.println("Bound parameter <"
    						+ parametersLooper + "> value : null") ;
    				preparedStatementToUse.setNull(parametersLooper + 1, 
        					(Integer)i_bindParams[0][parametersLooper]) ;
    			}
    		}
    		ResultSet resultOfQuery = preparedStatementToUse.executeQuery() ;
    		resultToReturn = this.convertResultSetToSSRResultSet(resultOfQuery) ;
    	}
    	catch(Exception e)
    	{
			// TODO : Do proper exception handling here
			System.out.println("Exception caught at point 6.1 in "
					+ "runCustomPreparedFetchQuery : \n" + e.getMessage()) ;
			e.printStackTrace(System.out) ;
    	}
    	return resultToReturn ;
    }
    
    public Object[][] convertResultSetToSSRResultSet(ResultSet i_resultSetToConvert) 
    		throws Exception
    {
		ResultSetMetaData queryResultMetaData = i_resultSetToConvert.getMetaData() ;
		int resultColumnCount = queryResultMetaData.getColumnCount() ;
		// The variable operationResult is not being used. It is only declared
		// so that return boolean value of various operations can be held and observed
		// during debugging, if required.
		boolean operationResult = true ;
		operationResult = i_resultSetToConvert.last() ;
		int resultRowCount = i_resultSetToConvert.getRow() ;
		operationResult = i_resultSetToConvert.first() ;
		Object[][] ssrResultSet = new Object[resultRowCount + 2][resultColumnCount] ;
		
		int columnLengthLoopVar = 0 ;
		int rowCountLoopVar = 0 ;
		
		for(columnLengthLoopVar = 1 ; columnLengthLoopVar <= resultColumnCount ; columnLengthLoopVar++)
		{
			ssrResultSet[0][columnLengthLoopVar - 1] = queryResultMetaData
					.getColumnLabel(columnLengthLoopVar);
			ssrResultSet[1][columnLengthLoopVar - 1] = queryResultMetaData
					.getColumnClassName(columnLengthLoopVar) ;
		}
		
		for(rowCountLoopVar = 0 ; rowCountLoopVar < resultRowCount ; rowCountLoopVar++)
		{
			for(columnLengthLoopVar = 1 ; columnLengthLoopVar <= resultColumnCount ; columnLengthLoopVar++)
			{
				ssrResultSet[rowCountLoopVar + 2][columnLengthLoopVar - 1] = 
						i_resultSetToConvert.getObject(columnLengthLoopVar) ;
			}
			operationResult = i_resultSetToConvert.next() ;
		}
		
		return ssrResultSet ;
    }

    public static String showFetchedDataRows(Object[][] i_databaseRowsToShow) 
            throws Exception
    {
        String toReturn = "" ;
        if(null == i_databaseRowsToShow)
        {
            toReturn = "<null>" ;
            return toReturn ;
        }
        for(int rowLoopVar = 0 ; rowLoopVar < i_databaseRowsToShow.length ; rowLoopVar++)
        {
            for(int columnLoopVar = 0 ; columnLoopVar < i_databaseRowsToShow[0].length ; columnLoopVar++)
            {
                toReturn += "\nRow[" + rowLoopVar + "]Column[" + columnLoopVar + "] : " ;
                Object oneValue = i_databaseRowsToShow[rowLoopVar][columnLoopVar] ;
                toReturn += (null == oneValue) ? "<null>" : oneValue.toString() ;
                toReturn += "\t (" 
                        + ((null == oneValue) 
                        ? "" 
                        : i_databaseRowsToShow[rowLoopVar][columnLoopVar].getClass().toString()) ;
                toReturn += ")" ;
            }
        }
        return toReturn ;
    }    
}
