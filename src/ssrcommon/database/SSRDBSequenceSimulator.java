/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrcommon.database;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author LENOVO
 */
public class SSRDBSequenceSimulator 
{
    protected static SSRDBSequenceSimulator ms_uniqueInstance ;
    
    protected HashMap<Map.Entry<String, String>, Long> m_sequenceHashMap ;
    
    protected SSRDBSequenceSimulator() throws Exception
    {
        this.m_sequenceHashMap = new HashMap<Map.Entry<String, String>, Long>() ;
    }
    
    public static SSRDBSequenceSimulator getInstance() throws Exception
    {
        SSRDBSequenceSimulator toReturn = null ;
        if(null == SSRDBSequenceSimulator.ms_uniqueInstance)
        {
            SSRDBSequenceSimulator.createUniqueInstance() ;
            toReturn = SSRDBSequenceSimulator.ms_uniqueInstance ;
        }
        else
        {
            toReturn = SSRDBSequenceSimulator.ms_uniqueInstance ;
        }
        return toReturn ;
    }
    
    public Long getNextUnusedColumnSequence(String i_tableName, String i_columnName) 
            throws Exception
    {
        Long toReturn = 0l ; // The second character is lowercase "L" for long
        String tableNameInLower = i_tableName.toLowerCase() ;
        String columnNameInLower = i_columnName.toLowerCase() ;
        Map.Entry<String, String> entryToCheckCache = new AbstractMap
                .SimpleEntry<String, String>(tableNameInLower, columnNameInLower) ;
        if(false == this.m_sequenceHashMap.containsKey(entryToCheckCache))
        {
            toReturn = this.getCurrentMaxValue(i_tableName, i_columnName) ;
            this.m_sequenceHashMap.put(entryToCheckCache, toReturn) ;
        }
        else
        {
            toReturn = this.m_sequenceHashMap.get(entryToCheckCache) ;
        }
        
        return toReturn ;
    }
    
    // The following method is assuming that even if a given table is empty the result
    // will still give the first two metadata rows
    protected Long getCurrentMaxValue(String i_tableName, String i_columnName) throws Exception
    {
        Long toReturn = 0l ; // That is 0 in long, the second character is small L not numeric one
        SSRBasicDBOperationsHandler dbOperationsHandler = new SSRBasicDBOperationsHandler() ;
        
        String queryToRun = " select max(" + i_columnName + ") from " + i_tableName + " ; " ;
        
        System.out.println("Query to be run : \n" + queryToRun) ;
        
        Object[][] maxColumnValueResult = dbOperationsHandler.runCustomQuery(queryToRun) ;
        
        // If there are no rows in the table, then return zero.
        if(3 != maxColumnValueResult.length)
            return toReturn ;
        
        if(true == maxColumnValueResult[1][0].getClass().equals(String.class))
        {
            queryToRun = " select max(cast(" + i_columnName + " as signed)) from " + i_tableName + " ; " ;
            maxColumnValueResult = dbOperationsHandler.runCustomQuery(queryToRun) ;
        }
        
        Object maxValue = maxColumnValueResult[2][0] ;
        toReturn = (null == maxValue) ? 0l : Long.parseLong(maxValue.toString()) ;
        System.out.println("getCurrentMaxValue DB operation result : \n" 
                + SSRBasicDBOperationsHandler.showFetchedDataRows(maxColumnValueResult)) ;
        
        return toReturn ;
    }
    
    private static synchronized void createUniqueInstance() throws Exception
    {
        if(null == SSRDBSequenceSimulator.ms_uniqueInstance)
        {
            SSRDBSequenceSimulator.ms_uniqueInstance = new SSRDBSequenceSimulator() ;
        }
    }
}
